package com.example.encryptiondecryption;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SubmitActivity extends AppCompatActivity {

    EditText ET_string;
    Button btn_submit;
    TextView TV_resultlabel;
    LinearLayout LL_parent;

    String action;
    String etString, resultString = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);

        ET_string = findViewById(R.id.ET_string);
        btn_submit = findViewById(R.id.btn_submit);
        TV_resultlabel = findViewById(R.id.TV_resultlabel);
        LL_parent = findViewById(R.id.LL_parent);

        ET_string.addTextChangedListener(filterTextWatcher);

        Bundle bundle = getIntent().getExtras();
        action = bundle.getString("action");

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()){
                    switch (action){

                        case "Encrypt":
                            encryption(etString);
                            break;

                        case "Decrypt":
                            decryption1(etString);
                            break;
                    }
                }
            }
        });

        LL_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
            }
        });
    }


    private boolean validate() {
        etString = ET_string.getText().toString();
        if (etString.trim().isEmpty()) {
            Toast.makeText(this, "Please enter any value.", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    private TextWatcher filterTextWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            TV_resultlabel.setText("");
            resultString = "";
        }
    };

    private void hideSoftKeyboard() {

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void encryption(String str)
    {
        int n = str.length();
        for (int i = 0; i < n; i++) {

            int count = 1;
            while (i < n - 1 && str.charAt(i) == str.charAt(i + 1)) {
                count++;
                i++;
            }

            resultString = resultString + str.charAt(i) + count;
            TV_resultlabel.setText(resultString);
        }
    }


    public  void decryption1(String s){
        int count = 0;
        boolean flag = true;
        StringBuilder result = new StringBuilder();
        char validOrNot = s.charAt(s.length() - 1);
        if (Character.isDigit(validOrNot)) {
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (Character.isLetter(c) || Character.isSpaceChar(c)) {
                    int countC = s.charAt(i + 1);
                    if (Character.isDigit(countC)) {
                        count = count * 10 + countC - '0';
                        while (count > 0) {
                            result.append(c);
                            count--;
                        }
                    } else {
                        flag = false;
                        break;
                    }
                }
            }
        }else {
            flag = false;
        }
        if (flag) {
            TV_resultlabel.setText(result.toString());
        }else {
            Toast.makeText(this, "Please enter a valid string to decrypt.", Toast.LENGTH_SHORT).show();
        }
    }

}
