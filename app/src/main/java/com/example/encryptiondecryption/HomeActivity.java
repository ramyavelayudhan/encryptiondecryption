package com.example.encryptiondecryption;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    Button btn_encrypt, btn_decrypt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btn_encrypt = findViewById(R.id.btn_encrypt);
        btn_decrypt = findViewById(R.id.btn_decrypt);

        btn_encrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent submitIntent = new Intent(HomeActivity.this, SubmitActivity.class);
                submitIntent.putExtra("action", "Encrypt");
                startActivity(submitIntent);

            }
        });

        btn_decrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent submitIntent = new Intent(HomeActivity.this, SubmitActivity.class);
                submitIntent.putExtra("action", "Decrypt");
                startActivity(submitIntent);

            }
        });
    }
}
